package nl.utwente.di.bookQuote;

public class Converter {
    double getTemperature(String celsius) {

        double doubleCelsius = Double.parseDouble(celsius);
        return doubleCelsius * 1.8 + 32;
    }
}
